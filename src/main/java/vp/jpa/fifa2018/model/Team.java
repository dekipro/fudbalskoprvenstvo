package vp.jpa.fifa2018.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(catalog = "db_jpa_fifa", name = "team")

public class Team {

	@Id
	@GeneratedValue
	private Long id;

	private String team_name;
	private String coach;

	@OneToMany(mappedBy = "homeTeam")
	Set<Match> homeTeamPlays;

	@OneToMany(mappedBy = "awayTeam")
	Set<Match> awayTeamPlays;

	public Team() {

	}

	public Team(Long id, String team_name, String coach) {
		super();
		this.id = id;
		this.team_name = team_name;
		this.coach = coach;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return team_name;
	}

	public void setName(String team_name) {
		this.team_name = team_name;
	}

	public String getCoach() {
		return coach;
	}

	public void setCoach(String coach) {
		this.coach = coach;
	}

	

	public Set<Match> getHomeTeamPlays() {
		return homeTeamPlays;
	}

	public void setHomeTeamPlays(Set<Match> homeTeamPlays) {
		this.homeTeamPlays = homeTeamPlays;
	}

	public Set<Match> getAwayTeamPlays() {
		return awayTeamPlays;
	}

	public void setAwayTeamPlays(Set<Match> awayTeamPlays) {
		this.awayTeamPlays = awayTeamPlays;
	}

	@Override
	public String toString() {
		return "Team [id=" + id + ", team_name=" + team_name + ", coach=" + coach + "]";
	}

}
