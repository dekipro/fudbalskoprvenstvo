package vp.jpa.fifa2018.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(catalog = "db_jpa_fifa", name = "football_match")
public class Match {
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	Team homeTeam;

	@ManyToOne(fetch = FetchType.EAGER)
	Team awayTeam;

	@ManyToOne(fetch = FetchType.EAGER)
	private Stadium stadium;

	private String score;

	public Match() {

	}

	public Match(Long id, Stadium stadium, String score) {
		super();
		this.id = id;
		this.stadium = stadium;
		this.score = score;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Stadium getStadium() {
		return stadium;
	}

	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "Match [id=" + id + ", homeTeam=" + homeTeam + ", awayTeam=" + awayTeam + ", stadium=" + stadium
				+ ", score=" + score + "]";
	}

	

}
