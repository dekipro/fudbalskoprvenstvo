package vp.jpa.fifa2018.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity

@Table(catalog = "db_jpa_fifa", name = "stadium") 
public class Stadium {
	@Id
	@GeneratedValue
	private Long id;
	
	private String stadium_name;
	private String location;
	private int capacity;
	
	@JsonIgnore 
	@OneToMany(mappedBy = "stadium", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Match> matches = new HashSet<Match>();
	
	public Stadium() {
		
	}

	public Stadium(Long id, String stadium_name, String location, int capacity) {
		super();
		this.id = id;
		this.stadium_name = stadium_name;
		this.location = location;
		this.capacity = capacity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return stadium_name;
	}

	public void setName(String stadium_name) {
		this.stadium_name = stadium_name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	
	
	public Set<Match> getMatches() {
		return matches;
	}

	public void setMatches(Set<Match> matches) {
		this.matches = matches;
	}

	@Override
	public String toString() {
		return "Stadium [id=" + id + ", stadium_name=" + stadium_name + ", location=" + location + ", capacity=" + capacity + "]";
	}
	
	
}
