package vp.jpa.fifa2018.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import vp.jpa.fifa2018.model.Team;



@Component
@Transactional 
public class TeamRepository {
	@PersistenceContext
	EntityManager em;
	
	public List<Team> findAll() {
		Query q = em.createQuery("SELECT t FROM Team t");

		return q.getResultList();
	}

	public Team findOne(Long id) {
		return em.find(Team.class, id);
	}

	public Team save(Team team) {
		if (team.getId() != null) {
			return em.merge(team);	
		} else {
			em.persist(team);
			return team;		
		}
		
	}
	
	public void delete(Long id) {
		em.remove(findOne(id));
	}
   
	public List<Team> findByNameContains(String name) {
		Query q = em.createQuery("SELECT t FROM Team t WHERE team_name LIKE :name");
		q.setParameter("name", "%" + name + "%"); 
		return q.getResultList();
	}
	
}
