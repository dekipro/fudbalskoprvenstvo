package vp.jpa.fifa2018.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.jpa.fifa2018.model.Match;
import vp.jpa.fifa2018.model.Stadium;
import vp.jpa.fifa2018.model.Team;

 
@Component
@Transactional
public class MatchRepository {
	@PersistenceContext
	EntityManager em;
	
	@Autowired
	StadiumRepository stadiumRepository;
	
	@Autowired
	TeamRepository teamRepository;
	
	public List<Match> findAll() {
		Query q = em.createQuery("SELECT m FROM Match m");

		return q.getResultList();
	}
	
	public Match findOne(Long id) {
		return em.find(Match.class, id);
	}
	
	public Match save(Match match) {
		em.persist(match);
		return match;		
	}
	
	public void delete(Long id) {
		em.remove(findOne(id));
	}
	
	public List<Match> findByStadiumId(Long stadiumId) {
		Stadium stadium = stadiumRepository.findOne(stadiumId);
		return new ArrayList<Match>(stadium.getMatches());
	}
	
	public List<Match> findByTeamId(Long teamId) {
		Team team = teamRepository.findOne(teamId);
		List<Match> allPlays = new ArrayList<Match>();
		allPlays.addAll(team.getHomeTeamPlays());
		allPlays.addAll(team.getAwayTeamPlays());
		return allPlays;
	}
	
	
}
