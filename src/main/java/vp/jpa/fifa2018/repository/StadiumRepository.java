package vp.jpa.fifa2018.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import vp.jpa.fifa2018.model.Stadium;



@Component
@Transactional 
public class StadiumRepository {

	@PersistenceContext
	EntityManager em;
	
	public List<Stadium> findAll() {
		Query q = em.createQuery("SELECT s FROM Stadium s");

		return q.getResultList();
	}
	
	public Stadium findOne(Long id) {
		return em.find(Stadium.class, id);
	}
	
	public Stadium save(Stadium stadium) {
		if (stadium.getId() != null) {
			return em.merge(stadium);	
		} else {
			em.persist(stadium);
			return stadium;		
		}
	}
	
	public void delete(Long id) {
		em.remove(findOne(id));
	}
	
	public List<Stadium> findByNameContains(String name) {
		Query q = em.createQuery("SELECT s FROM Stadium s WHERE stadium_name LIKE :name");
		q.setParameter("name", "%" + name + "%"); 
		return q.getResultList();
	}
}
