package vp.jpa.fifa2018.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.jpa.fifa2018.model.Team;
import vp.jpa.fifa2018.service.TeamService;



@RestController

public class TeamController {
	@Autowired
	TeamService teamService;
	
	@RequestMapping(value = "api/teams", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Team>> getAllTeams() {
		
		List<Team> teams = teamService.findAll();	
		
		return new ResponseEntity<>(teams, HttpStatus.OK); 
	}
	
	@RequestMapping(value = "api/teams/{id}", method = RequestMethod.GET)
	public ResponseEntity<Team> getStudent(@PathVariable Long id) {
		Team team = teamService.findOne(id);

		return new ResponseEntity<>(team, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/teams", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Team> create(@RequestBody Team team) {
		Team retVal = teamService.save(team);

		return new ResponseEntity<>(retVal, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "api/teams/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Team> update(@PathVariable Long id,
			@RequestBody Team team) {
		team.setId(id);
		Team retVal = teamService.save(team);

		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/teams/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Team team = teamService.findOne(id);
		if (team != null) {
			teamService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "api/teams", method = RequestMethod.GET, params = "team_name")
	public ResponseEntity<List<Team>> getTeamsByName(
			@RequestParam String team_name) {
		List<Team> teams = teamService.findByName(team_name);

		return new ResponseEntity<>(teams, HttpStatus.OK);
	}
}
