package vp.jpa.fifa2018.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vp.jpa.fifa2018.model.Match;
import vp.jpa.fifa2018.service.MatchService;




public class MatchController {
	@Autowired
	MatchService matchService;
	
	@RequestMapping(value = "api/matches", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Match>> getAllMatches() {
		
		List<Match> matches = matchService.findAll();	
		
		return new ResponseEntity<>(matches, HttpStatus.OK); 
	}
	
	@RequestMapping(value = "api/matches/{id}", method = RequestMethod.GET)
	public ResponseEntity<Match> getMatch(@PathVariable Long id) {
		Match match = matchService.findOne(id);

		return new ResponseEntity<>(match, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/matches", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Match> create(@RequestBody Match match) {
		Match retVal = matchService.save(match);

		return new ResponseEntity<>(retVal, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "api/matches/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Match> update(@PathVariable Long id,
			@RequestBody Match match) {
		match.setId(id);
		Match retVal = matchService.save(match);

		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/matches/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Match match = matchService.findOne(id);
		if (match != null) {
			matchService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "api/stadiums/{stadiumId}/matches", method = RequestMethod.GET)
	public ResponseEntity<List<Match>> findStadiumtMatches(@PathVariable Long stadiumId) {
		List<Match> matches = matchService.findByStadiumId(stadiumId);
		
		return new ResponseEntity<>(matches, HttpStatus.OK); 
	}
	
	@RequestMapping(value = "api/teams/{teamId}/matches", method = RequestMethod.GET)
	public ResponseEntity<List<Match>> findTeamMatches(@PathVariable Long teamId) {
		List<Match> matches = matchService.findByTeamId(teamId);
		
		return new ResponseEntity<>(matches, HttpStatus.OK); 
	}
}
