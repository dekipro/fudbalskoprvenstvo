package vp.jpa.fifa2018.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.jpa.fifa2018.model.Stadium;
import vp.jpa.fifa2018.service.StadiumService;



@RestController

public class StadiumController {
	@Autowired
	StadiumService stadiumService;
	
	@RequestMapping(value = "api/stadiums", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Stadium>> getAllStadiums() {
		
		List<Stadium> teams = stadiumService.findAll();	
		
		return new ResponseEntity<>(teams, HttpStatus.OK); 
	}
	
	@RequestMapping(value = "api/stadiums/{id}", method = RequestMethod.GET)
	public ResponseEntity<Stadium> getStadium(@PathVariable Long id) {
		Stadium stadium = stadiumService.findOne(id);

		return new ResponseEntity<>(stadium, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/stadiums", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Stadium> create(@RequestBody Stadium stadium) {
		Stadium retVal = stadiumService.save(stadium);

		return new ResponseEntity<>(retVal, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "api/stadiums/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Stadium> update(@PathVariable Long id,
			@RequestBody Stadium stadium) {
		stadium.setId(id);
		Stadium retVal = stadiumService.save(stadium);

		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/stadiums/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Stadium stadium = stadiumService.findOne(id);
		if (stadium != null) {
			stadiumService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "api/stadiums", method = RequestMethod.GET, params = "stadium_name")
	public ResponseEntity<List<Stadium>> getStadiumsByName(
			@RequestParam String stadium_name) {
		List<Stadium> stadiums = stadiumService.findByName(stadium_name);

		return new ResponseEntity<>(stadiums, HttpStatus.OK);
	}
}
