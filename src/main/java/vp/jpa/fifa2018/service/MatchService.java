package vp.jpa.fifa2018.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.jpa.fifa2018.model.Match;
import vp.jpa.fifa2018.repository.MatchRepository;

@Component
public class MatchService {
	@Autowired
	MatchRepository matchRepository;
	
	public List<Match> findAll() {
		return matchRepository.findAll();
	}
	
	public Match findOne(Long id) {
		return matchRepository.findOne(id);
	}
	
	public Match save(Match match) {
		return matchRepository.save(match);
	}

	public void remove(Long id) {
		
		matchRepository.delete(id);	
	}
	
	public List<Match> findByStadiumId(Long stadiumId){
		return matchRepository.findByStadiumId(stadiumId);
	}
	
	public List<Match> findByTeamId(Long teamId) {
		return matchRepository.findByTeamId(teamId);
	}
	
}
