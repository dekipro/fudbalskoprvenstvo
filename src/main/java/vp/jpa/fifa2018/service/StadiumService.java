package vp.jpa.fifa2018.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.jpa.fifa2018.model.Stadium;
import vp.jpa.fifa2018.repository.StadiumRepository;

 
@Component
public class StadiumService {
	@Autowired
	StadiumRepository stadiumRepository;
	
	public List<Stadium> findAll() {
		return stadiumRepository.findAll();
	}
	
	public Stadium findOne(Long id) {
		return stadiumRepository.findOne(id);
	}
	
	public Stadium save(Stadium stadium) {
		return stadiumRepository.save(stadium);
	}

	public void remove(Long id) {
		
		stadiumRepository.delete(id);
		
	}
	
	public List<Stadium> findByName(String name) {
		return stadiumRepository.findByNameContains(name);
	}
}
