package vp.jpa.fifa2018.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.jpa.fifa2018.model.Team;
import vp.jpa.fifa2018.repository.TeamRepository;


@Component
public class TeamService {
	@Autowired
	TeamRepository teamRepository;
	
	public List<Team> findAll() {
		return teamRepository.findAll();
	}
	
	public Team findOne(Long id) {
		return teamRepository.findOne(id);
	}
	
	public Team save(Team team) {
		return teamRepository.save(team);
	}

	public void remove(Long id) {
		
		teamRepository.delete(id);
		
	}
	
	public List<Team> findByName(String name) {
		return teamRepository.findByNameContains(name);
	}

}
